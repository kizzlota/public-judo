import is from 'is_js';
import * as Const from '../const/PageInfo';
import {LacalesList, LANG_DEFAULT} from './vars';
import Locales from '../locale/index';
import axios from 'axios';
import {PUBLICK_STATUS} from '../const/PageInfo';
import {COOCKIE_TIME_LIVE} from '../const/PageInfo';
import {PROTECTED_STATUS} from '../const/PageInfo';
import {checkToken, api_user_token, full_domain} from '../urls';
import {logoutAction} from '../components/user/actions';

function format() {
    let formatted = this;
    for (let arg in arguments) {
        formatted = formatted.replace('{' + arg + '}', arguments[arg]);
    }
    return formatted;
}

String.prototype.format = format;

function getAccess(element) {
    let {access_status} = element;
    return is.equal(access_status, PUBLICK_STATUS);
}

let changePageName = (dispatch, pageName) => {
    if
    (
        dispatch &&
        is.not.undefined(dispatch) &&
        is.function(dispatch) &&
        is.string(pageName) &&
        is.not.empty(pageName)
    ) {
        return dispatch({type: Const.CHANGE_PAGE, pageName});
    }
    return false;
};

let localeSwitch = (dispatch, lang) => {
    if
    (
        dispatch &&
        is.not.undefined(dispatch) &&
        is.function(dispatch) &&
        is.string(lang) &&
        is.not.empty(lang)
    ) {
        return dispatch({type: Const.CHANGE_LANG, lang});
    }
    return false;
};

let getLangurdgeProps = (props) => {
    if (
        props &&
        is.not.undefined(props) &&
        is.not.empty(props) &&
        props.hasOwnProperty('match')
    ) {
        let {lang} = props.match.params;
        return (is.inArray(lang, LacalesList)) ? lang : LANG_DEFAULT;
    }

    return LANG_DEFAULT;
};


let getPathProps = (props, param) => {
    if (
        props &&
        is.not.undefined(props) &&
        is.not.empty(props) &&
        props.hasOwnProperty('match')
    ) {
        let {path} = props.match;
        path = (path.split('/'));
        path = path[(path.length - 1)];
        return (is.not.undefined(path) && is.not.empty(path)) ? path : '/';
    }
    return '/';
};

let getPageName = (props) => {
    if (
        props &&
        is.not.undefined(props) &&
        is.not.empty(props) &&
        props.hasOwnProperty('match')
    ) {
        let {url} = props.match;
        url = (url.split('/'));
        let pageName = url.length > 0 ? url[2] : '';
        return (is.not.undefined(pageName) && is.not.empty(pageName)) ? pageName : '/';
    }
    return '/';
};

let getPathForUrl = (param) => {
    let structureUrl = {
        lang: '',
        page: '',
        params: ''
    };
    let loc = (location.pathname.replace(/^\/|\/$/g, '')).split('/');
    let index = 0, indexPath = 1;
    if (is.not.empty(loc) && structureUrl.hasOwnProperty(param)) {
        let lang = loc[index];
        if (is.not.inArray(lang, LacalesList)) {
            indexPath = 0;
        }
        let path = loc[indexPath];
        let params = loc[(indexPath + 1)];
        structureUrl = {
            lang: ((is.not.undefined(lang) && is.not.empty(lang) && is.inArray(lang, LacalesList)) ? lang : LANG_DEFAULT),
            page: ((is.not.undefined(path) && is.not.empty(path)) ? path : ''),
            params: ((is.not.undefined(params) && is.not.empty(params)) ? params : '')
        };
        return structureUrl[param];
    }
    return false;
};

let testUrlForDefLang = () => {
    let params = (location.pathname.replace(/^\/|\/$/g, '')).split('/');
    return (is.inArray(LANG_DEFAULT, params));
};

let createPathLocale = (namePage) => {
    let lang = getPathForUrl('lang');
    let path = '';
    if (is.undefined(namePage)) {
        let pathUrl = getPathForUrl('page');
        pathUrl = (pathUrl && is.not.undefined(pathUrl)) ? pathUrl : '';
        let paramUrl = getPathForUrl('params');
        paramUrl = (paramUrl && is.not.undefined(paramUrl)) ? paramUrl : '';
        path = pathUrl + ((paramUrl) ? `/${paramUrl}` : '');
    }
    if (is.not.undefined(lang) && is.not.empty(lang) && is.not.undefined(namePage)) {
        let ClearPageName = namePage.replace(/^\/|\/$/g, '');
        if (lang === LANG_DEFAULT) {
            path = !testUrlForDefLang() ? `/${ClearPageName}` : `/${lang}/${ClearPageName}`;
        } else {
            path = (is.inArray(lang, LacalesList)) ? `/${lang}/${ClearPageName}` : `/${ClearPageName}`;
        }
    }
    return path;
};

let getLocalePage = (namePage) => {
    let lang = getPathForUrl('lang');
    if (
        is.not.undefined(namePage) &&
        is.not.undefined(lang) &&
        is.not.undefined(Locales[lang])) {
        return Locales[lang][namePage];
    }
    return null;
};


function readCookie(name) {
    return (name = new RegExp('(?:^|;\\s*)' + ('' + name).replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&') + '=([^;]*)').exec(document.cookie)) && name[1];
}

function setCookie(name, value, props = {}) {
    let exp = props.expires;

    if (typeof exp === 'number' && exp) {
        let d = new Date();

        d.setTime(d.getTime() + exp * 1000);

        exp = props.expires = d;
    }

    if (exp && exp.toUTCString) {
        props.expires = exp.toUTCString();
    }

    value = encodeURIComponent(value);

    let updatedCookie = name + '=' + value;

    for (let propName in props) {
        updatedCookie += '; ' + propName;

        let propValue = props[propName];

        if (propValue !== true) {
            updatedCookie += '=' + propValue;
        }
    }
    document.cookie = updatedCookie;
}

function deleteCookie(name) {
    setCookie(name, null, {expires: -1});
    if (name === 'token')
        document.cookie = 'token' + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

function updateToken(token = '') {
    if (is.not.empty(token) && is.not.falsy(token)) {
        setCookie('token', token, {expires: COOCKIE_TIME_LIVE});
    } else {
        token = readCookie('token');
        if (is.not.empty(token) && is.not.falsy(token)) {
            updateToken(token);
        }
    }
}


function updateLocales(dispatch, pagename) {
    let lang = getPathForUrl('lang');
    if (!localeSwitch(dispatch, lang)) {
        console.error(`error change localeSwitch ${pagename}`);
    }
}

function getDate(data) {
    let lang = getPathForUrl('lang');
    let day = is.not.empty(data) ? new Date(data) : '';
    let locales = {'en': 'en-US', 'ua': 'uk-UA'};
    let locale = locales[lang];
    if (is.empty(locale)) {
        locale = 'en-US';
    }
    return is.not.string(day) ? `${day.getUTCDate()} - ${day.toLocaleString(locale, {month: 'long'})} - ${day.getFullYear()}` : '';
}

function getAccessChildren(element) {
    let {access_status} = element.type;
    return is.equal(PROTECTED_STATUS, access_status);
}

function logout(status, dispatch, statusOpen = false) {
    if (status === 401 && is.function(dispatch) && statusOpen) {
        dispatch(logoutAction());
        return false;
    }
    return true;
}

axios.defaults.baseURL = full_domain;


function defaultParams(method = 'post', url) {
    try {
        if (api_user_token === url) {
            deleteCookie('token');
        }
        let token = readCookie('token');
        let options = {
            method: method,
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json; charset=utf-8'
            }
        };
        if (is.not.empty(token) && is.not.falsy(token)) {
            updateToken(token);
            options.headers.Authorization = `Token  ${token}`;
        }
        return options;
    } catch (error) {
        console.log('ERROR', error);
        return {};
    }
}


async function Post(url, params = {}, dispatch, statusOpen) {
    try {
        let options = defaultParams('post', url);
        if (is.not.empty(params)) {
            options.data = params;
        }

        const res = await axios(url, options);
        let result = await res;
        let {data, status} = result;
        logout(status, dispatch, statusOpen);
        return {data, status};
    } catch (error) {
        if (is.not.undefined(error.response)) {
            let {data = {}, status = 400} = error.response;
            logout(status, dispatch, statusOpen);
            return {data, status};
        }

        return {data: {'ERROR': error}, status: 400};
    }
}

async function Update(url, params = {}, dispatch, statusOpen) {
    try {
        let options = defaultParams('put', url);
        if (is.not.empty(params)) {
            options.data = params;
        }

        const res = await axios(url, options);
        let result = await res;
        let {data, status} = result;
        logout(status, dispatch, statusOpen);
        return {data, status};
    } catch (error) {
        if (is.not.undefined(error.response)) {
            let {data = {}, status = 400} = error.response;
            logout(status, dispatch, statusOpen);
            return {data, status};
        }

        return {data: {'ERROR': error}, status: 400};
    }
}


async function Delete(url, params = {}, dispatch, statusOpen) {
    try {
        let options = defaultParams('delete', url);
        if (is.not.empty(params)) {
            options.data = params;
        }

        const res = await axios(url, options);
        let result = await res;
        let {data, status} = result;
        logout(status, dispatch, statusOpen);
        return {data, status};
    } catch (error) {
        if (is.not.undefined(error.response)) {
            let {data = {}, status = 400} = error.response;
            logout(status, dispatch, statusOpen);
            return {data, status};
        }

        return {data: {'ERROR': error}, status: 400};
    }
}


async function Get(url, params = {}, dispatch, statusOpen) {
    try {
        let options = defaultParams('get', url);
        const res = await axios(url, options);
        let result = await res;
        let {data, status} = result;
        logout(status, dispatch, statusOpen);
        return {data, status};
    } catch (error) {
        if (is.not.undefined(error.response)) {
            let {data = {}, status = 400} = error.response;
            logout(status, dispatch, statusOpen);
            return {data, status};
        }

        return {data: {'ERROR': error}, status: 400};
    }
}

async function getToken() {
    let token = readCookie('token');
    if (is.not.empty(token) && is.not.falsy(token)) {
        let res = Post(checkToken);
        return await res;
    }
    return await false;
}

export {
    changePageName, // зміна імя активної сторінки в сторі
    localeSwitch, // зміна location
    getLangurdgeProps, // зчитує мову з параматрів роутінга в пропсах
    getPathProps, // зчитує шлях
    createPathLocale, // сформувати урл  на сторінки з відповідною локалізацією
    getPathForUrl, // зчитуваня параметрів з урла
    testUrlForDefLang,
    getLocalePage, // читає локалізації конкретної сторінки
    Post,
    Get,
    Update,
    Delete,
    getAccess,
    readCookie,
    setCookie,
    deleteCookie,
    updateToken,
    getAccessChildren,
    getToken,
    getPageName,
    updateLocales,
    getDate
};
