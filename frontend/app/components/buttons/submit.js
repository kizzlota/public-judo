import React from 'react';
import is from 'is_js';

let ButtonSubmit = (text = 'Submit', disabled = false, addClass = '') => {
    let params = {};
    if (disabled) {
        params.disabled = true;
    }
    return (
        <button
            {...params}
            className={`bottom-submit btn${is.not.empty(addClass) ? ` ${addClass}` : ''}`}
            type='submit'
        >
            {text}
        </button>
    );
};

export default ButtonSubmit;
