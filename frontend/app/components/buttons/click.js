import React from 'react';
import is from 'is_js';

let ButtonClick = (dispatch, action = {}, text = '', disabled, icon = 'fi-target-two', addClass = '') => {
    let params = {
        type: 'button',
        className: `bottom-click${is.not.empty(addClass) ? ` ${addClass}` : ''}`
    };
    params = is.function(dispatch) ? {...params, onClick: () => dispatch(action)} : params;
    params = disabled ? {...params, disabled: true} : params;
    return (
        <button
            {...params}
        >
            <span>{text}</span>
            <i className={icon}/>
        </button>
    );
};

export default ButtonClick;
