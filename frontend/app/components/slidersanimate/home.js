import React, {Component} from 'react';
import {GLOBAL} from '../../config';
import is from 'is_js';
import {connect} from 'react-redux';
import * as Const from '../../const/PageInfo';

class HomeAnimate extends Component {
    stop = false;

    constructor(props) {
        super(props);
        this.state = {play: true};
    }

    createLi(el, index) {
        return (
            <li
                className="literal-animate"
                key={index}
            >
                {el}
            </li>);
    }

    componentDidMount() {
        let _this = this;
        let {ANIMATE_TIME_CHANGE_PAGE} = GLOBAL;
        this.stop = setTimeout(() => {
            _this.setState({play: false});
        }, ANIMATE_TIME_CHANGE_PAGE);
    }

    componentWillUnmount() {
        if (this.stop) {
            clearTimeout(this.stop);
            this.stop = false;
        }
    }

    render() {
        let {children, pageName, access, statusOpen} = this.props;
        pageName += '...';
        let anim_text = pageName.split('');
        let {play} = this.state;
        return (
            is.not.empty(children) && is.not.undefined(children)
                ?
                play
                    ?
                    access || statusOpen
                        ?
                        <ul
                            className="animate-body-string animated fadeInUp"
                        >
                            {anim_text.map(::this.createLi)}
                        </ul>
                        :
                        null
                    :
                    <div>
                        {children}
                    </div>
                :
                null
        );
    }
}

function mapStateToProps(state) {
    return {
        statusOpen: state.userData.statusOpen
    };
}

export default connect(mapStateToProps)(HomeAnimate);

