import React, {Component} from 'react';
import {connect} from 'react-redux';
import {openForm} from '../loginForm/actions';
import {logoutAction} from './actions';
import ButtonClick from '../buttons/click';
import {GLOBAL} from '../../config';

class User extends Component {
    stop = false;
    remove = false;

    constructor(props) {
        super(props);
        this.state = {
            hover_icon: false,
            open: false,
            block: false,
            close: true
        };
    }

    openInfoBox() {
        let {open, close} = this.state;
        let {statusOpen} = this.props;
        let _this = this;
        let state = {open: !open};
        if (close) {
            state.close = false;
        }
        let time_remove = 500;
        let stop = () => {
            if (statusOpen) {
                if (state.open) {
                    this.stop = setTimeout(() => _this.setState({open: false}, () => setTimeout(() => _this.setState({close: true}), time_remove)), 25000);
                } else {
                    this.remove = setTimeout(() => _this.setState({close: true}), time_remove);
                }
            }
        };
        if (!state.open && this.stop) {
            clearTimeout(this.stop);
            this.stop = false;
        }
        if (state.open && this.remove) {
            clearTimeout(this.remove);
            this.remove = false;
        }
        this.setState(state, () => stop());
    }

    openLoginForm() {
        let {dispatch} = this.props;
        dispatch(openForm(true));
    }

    changeSelfHover() {
        let _this = this;
        let stop = () => setTimeout(() => _this.setState({hover_icon: false}), 2000);
        this.setState({hover_icon: true}, stop);
    }

    logout() {
        let {dispatch} = this.props;
        this.setState({open: false, close: true});
        let {deleteCookie} = GLOBAL;
        deleteCookie('token');
        dispatch(logoutAction());
    }

    clearIntrervalsState() {
        if (this.stop) {
            clearTimeout(this.stop);
            this.stop = false;
        }
        if (this.remove) {
            clearTimeout(this.remove);
            this.remove = false;
        }
    }

    componentWillUnmount() {
        ::this.clearIntrervalsState();
    }

    render() {
        let {statusOpen, userInfo, locale} = this.props;
        let {hover_icon, open, close} = this.state;
        let {email = ''} = userInfo;
        let toogle;
        if (statusOpen) {
            toogle = open ? 'open' : 'close';
            if (toogle === 'close') {
                ::this.clearIntrervalsState();
            }
        }
        return (
            statusOpen
                ?
                <div
                    className="user-container"
                >
                    <i
                        title={locale.user_info}
                        className="fi-torso medium border-on user-icon-small"
                        onClick={::this.openInfoBox}
                    />
                    {
                        !close
                            ?
                            <div
                                onMouseLeave={open && ::this.openInfoBox}
                                className={`userInfo animated ${toogle === 'open' ? ' fadeInRight' : (toogle === 'close' ? ' fadeOutRight' : '')}`}
                            >
                                <header>
                                    <h3>{locale.profile_info}</h3>
                                </header>
                                <section>
                                    <p><span>email:</span>{email}</p>
                                    <p>USER INFO PROFILE</p>
                                </section>
                                <section>
                                    {
                                        ButtonClick(
                                            ::this.logout,
                                            {},
                                            locale.logout,
                                            false,
                                            'fi-arrow-right',
                                            'abs-bottom-right'
                                        )
                                    }
                                </section>
                            </div>
                            :
                            null
                    }

                </div>
                :
                <div
                    className="user-container"
                >
                    <div
                        className="user-icon"
                    >
                        <div
                            onClick={::this.openLoginForm}
                            onMouseOver={::this.changeSelfHover}
                        >
                            <span
                                className={`small icon ${hover_icon && 'on-hover'}`}
                            >
                                <i
                                    className='fi-key small'
                                />
                                {locale.login}
                            </span>
                        </div>
                    </div>
                </div>
        );
    }
}


function mapStateToProps(state) {
    return {
        locale: state.langPage.UserLocales,
        statusOpen: state.userData.statusOpen,
        userInfo: state.userData.userInfo
    };
}

export default connect(mapStateToProps)(User);
