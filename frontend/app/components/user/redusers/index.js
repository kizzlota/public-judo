import {OPEN_STATUS, USER_INFO, AUTH_TRUE} from '../../../const/PageInfo';

let initialState = {userInfo: {}, statusOpen: false};

function statusAuthUser(state = initialState, action ) {
    switch (action.type) {
        case OPEN_STATUS: {
            return {...state, statusOpen: action.statusOpen};
        }
        case USER_INFO: {
            return {...state, userInfo: action.userInfo};
        }
        case AUTH_TRUE: {
            let {userInfo, statusOpen} = action;
            return {...state, userInfo, statusOpen};
        }
        default:
            return state;
    }
}

export default statusAuthUser;
