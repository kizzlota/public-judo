import {LOGIN_STATUS} from '../../../const/PageInfo';


function loginFormTougle(state = false, action) {
    switch (action.type) {
        case LOGIN_STATUS: {
            let {statusFormLogin} = action;
            return statusFormLogin;
        }
        default:
            return false;
    }
}

export default loginFormTougle;
