/* eslint-disable camelcase */
import React, {Component} from 'react';
import {GLOBAL} from '../../config';
import is from 'is_js';
import {connect} from 'react-redux';
import {openForm} from '../loginForm/actions';
import Login from '../loginForm';
import {authSuccess} from '../user/actions';
import {AnimScripts} from '../../components/slidersanimate';
import Header from '../../components/body/header';
import Footer from '../../components/body/Footer';
import CenterContainer from '../../components/body/CenterColumn';
import User from '../../components/user/index';
import {HomeStart} from '../../config/vars';
import {logoutAction} from '../user/actions';


class RouteLoaderPage extends Component {
    animated_script = 'standart';
    awaitStatus = false;
    getAccessChildren = false;

    constructor(props) {
        super(props);
        this.state = {
            open: false
        };
    }

    getHeaderContent() {
        return <User/>;
    }

    // shouldComponentUpdate(nextProps, nextState) {
    //     console.log('nextProps', nextProps);
    //     return nextProps.switcherMenu;
    // }

    componentWillMount() {
        let {dispatch} = this.props;
        let {updateLocales} = GLOBAL;
        updateLocales(dispatch, 'RouteLoaderPage');
    }

    async check() {
        let {getToken} = GLOBAL;
        let {dispatch} = this.props;
        if (this.awaitStatus) {
            return;
        }
        this.awaitStatus = true;
        let result = await getToken();
        let {status, data} = result;
        if (status === 200) {
            this.awaitStatus = false;
            let {email = ''} = data;
            dispatch(authSuccess({statusOpen: true, userInfo: {email: email}}));
        } else {
            this.awaitStatus = false;
            if (result || this.access_status)
                dispatch(openForm(true));
        }
    }

    getAccess() {
        let {statusOpen} = this.props;
        (statusOpen && !this.awaitStatus) || ::this.check();
    }

    componentDidMount() {
        ::this.getAccess();
    }

    componentDidUpdate() {
        let {readCookie} = GLOBAL;
        let {statusOpen, dispatch} = this.props;
        if (statusOpen && !readCookie('token')) {
            dispatch(logoutAction());
        }
        ::this.getAccess();
    }

    getAnimScript() {
        let {children} = this.props;
        let {animated_script} = children.type;
        if (is.not.empty(animated_script) && is.string(animated_script)) {
            this.animated_script = animated_script;
        }
        let animated = AnimScripts[this.animated_script];
        return is.not.undefined(animated) && is.not.empty(animated) ? animated : false;
    }

    getLogin() {
        let {children, locale = {RouteLoaderPage: {}}} = this.props;
        let {RouteLoaderPage: {access_denied = {}}} = locale;
        let {pageName = HomeStart} = children.type;
        let Animated = ::this.getAnimScript();
        let {getParamsColumns} = GLOBAL;
        let headerParam = getParamsColumns('header');
        let FooterParam = getParamsColumns('footer');
        let CenterContainerParam = getParamsColumns();
        headerParam.visability = true;
        FooterParam.visability = true;
        CenterContainerParam.visability = true;
        CenterContainerParam.proportions = '_full';
        CenterContainerParam.proportionsVertical = '';
        headerParam.pageName = pageName;
        headerParam.activeLink = pageName;
        headerParam.pageInfo = {};
        headerParam.pageInfo = {pageName: pageName};
        headerParam.child = ::this.getHeaderContent();
        if (is.not.empty(access_denied) && is.string(access_denied)) {
            access_denied = access_denied.format(`"${pageName}"`);
        }
        return (
            <div
                className="containerPage"
            >
                <div
                    id="bodyContainer"
                >
                    <Header
                        {
                        ...headerParam
                        }
                    />
                    <CenterContainer
                        {...CenterContainerParam}
                    >
                        <Animated
                            access={!this.access_status}
                            pageName={pageName}
                        >
                            {
                                is.not.empty(access_denied)
                                &&
                                <div>
                                    <div
                                        className="access-denied-info"
                                    >
                                        <p>{access_denied}</p>
                                    </div>
                                    <div
                                        className='access-logo'
                                    >
                                        <p className='fi-key'/>
                                        {/* <p className='fi-shield'/>*/}
                                    </div>
                                </div>
                            }
                        </Animated>
                    </CenterContainer>
                    <Footer
                        {...FooterParam}
                    />
                </div>
            </div>

        );
    }

    render() {
        let {children, statusOpen} = this.props;
        let {getAccessChildren} = GLOBAL;
        let Animated = ::this.getAnimScript();
        let {pageName = 'Change Page'} = children.type;
        this.access_status = getAccessChildren(children);
        return (
            <div>
                {

                    statusOpen === this.access_status || !this.access_status
                        ?
                        Animated
                            ?
                            <Animated
                                access={!this.access_status}
                                pageName={pageName}
                            >
                                {children}
                            </Animated>
                            :
                            children
                        :
                        ::this.getLogin()
                }
                {
                    this.access_status && !statusOpen
                        ?
                        <Login/>
                        :
                        null
                }
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        locale: state.langPage.bodyLocale,
        statusOpen: state.userData.statusOpen,
        switcherMenu: state.switcherMenu
    };
}

export default connect(mapStateToProps)(RouteLoaderPage);
