import React, { Component} from 'react';
import PropTypes from 'prop-types';
import Logo from './logo';
import Menu from '../Menu';

export default class Header extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        let {visability, iconStatus, iconStyle, changePage, pageInfo, child} = this.props;
        return (
            visability
                ?
                <header id="header">
                    {
                        iconStatus
                            ?
                            <Logo
                                visability = {iconStatus}
                                icon = {iconStyle}
                            />
                            :
                            null
                    }
                    <Menu
                        changePage={changePage}
                        pageInfo={pageInfo}
                    />
                    {child}
                </header>
                :
                null

        );
    }
}

Header.defaultProps = {
    visability: true,
    iconStatus: true,
    iconStyle: 'standart'
};


Header.PropTypes = {
    visability: PropTypes.bool.isRequired, // відображення
    iconStatus: PropTypes.bool.isRequired, // відображення логотипа
    iconStyle: PropTypes.string // який логотип
};
