import React, {Component} from 'react';
import PropTypes from 'prop-types';
import is from 'is_js';

export default class LeftContainer extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let {visability, children, child, proportionsVertical, class_add = ''} = this.props;
        return (
            visability
                ?
                <div
                    id="LeftContainer"
                    className={(is.not.empty(proportionsVertical) ? `vert_size${proportionsVertical}` : '') + (is.not.empty(class_add) ? ` ${class_add}` : '')}
                >
                    {child}
                    {children}
                </div>
                :
                null
        );
    }
}

LeftContainer.defaultProps = {
    visability: false,
    child: null
};


LeftContainer.PropTypes = {
    visability: PropTypes.bool.isRequired,
    child: PropTypes.object.isRequired
};

