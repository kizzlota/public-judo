import React, {Component} from 'react';
import HomePage from '../../pages/Home/index';
import TodosPage from '../../pages/Task/index';
import {Link} from 'react-router-dom';
import {GLOBAL} from '../../config';
import {HomeStart} from '../../config/vars';
import {connect} from 'react-redux';
import is from 'is_js';

class Menu extends Component {
    Pages = [
        HomePage,
        TodosPage
    ];

    constructor(props) {
        super(props);
        this.animateChange = this.animateChange.bind(this);
    }

    getItems() {
        // let {statusOpen} = this.props;
        let items = [];
        // let {getAccess} = GLOBAL;
        let filterItems = this.Pages;
        // if (!statusOpen) {
        //     filterItems = filterItems.filter(getAccess);
        // }
        for (let numPage in filterItems) {
            items.push(this.Pages[numPage].getInfo());
        }
        return items;
    }

    animateChange(linkActive) {
        let {changePage} = this.props;
        if (is.function(changePage))
            changePage();
    }

    render() {
        let items = this.getItems();
        let {pageInfo} = this.props;
        let activePageName = pageInfo.pageName;
        let duration = 0;
        return (
            <nav
                id="navContainer"
            >
                <ul className="linkContainer">
                    {
                        items.map((data, index) =>
                            <li
                                key={index}
                                value={data.value}
                                style={{'animationDelay': `${duration + (index / 5)}s`}}
                                className={activePageName === data.value ? 'animated fadeInDown active' : 'animated fadeInDown'}
                            >
                                {activePageName !== data.value ?
                                    <Link
                                        to={HomeStart !== data.value ? GLOBAL.createPathLocale(data.value) : GLOBAL.createPathLocale('/')}
                                        onClick={this.animateChange}
                                    >
                                        {data.namePage}
                                    </Link>
                                    :
                                    data.namePage
                                }
                            </li>
                        )}
                </ul>
            </nav>
        );
    }
}


function mapStateToProps(state) {
    return {
        statusOpen: state.userData.statusOpen,
        switcherMenu: state.switcherMenu
    };
}

export default connect(mapStateToProps)(Menu);
