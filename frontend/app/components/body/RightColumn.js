import React, {Component} from 'react';
import PropTypes from 'prop-types';
import is from 'is_js';

export default class RightContainer extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        let {visability, child, proportionsVertical} = this.props;
        return (
            visability
                ?
                <div
                    id="RightContainer"
                    className={(is.not.empty(proportionsVertical) ? `vert_size${proportionsVertical}` : '')}
                >
                    <h1>RightContainer</h1>
                    {child}
                </div>
                :
                null
        );
    }
}

RightContainer.defaultProps = {
    visability: false,
    child: null
};


RightContainer.PropTypes = {
    visability: PropTypes.bool.isRequired,
    child: PropTypes.object.isRequired
};
