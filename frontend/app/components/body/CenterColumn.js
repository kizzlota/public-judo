import React, {Component} from 'react';
import PropTypes from 'prop-types';
import is from 'is_js';

export default class CenterContainer extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        let {visability, child, proportions, proportionsVertical, children} = this.props;
        return (
            visability
                ?
                <div
                    id="CenterContainer"
                    className={`centr_size${proportions}${(is.not.empty(proportionsVertical) ? ` vert_size${proportionsVertical}` : '')}`}
                >
                    {child}
                    {children}
                </div>
                :
                null
        );
    }
}

CenterContainer.defaultProps = {
    visability: false,
    child: null
};


CenterContainer.PropTypes = {
    visability: PropTypes.bool.isRequired,
    child: PropTypes.object.isRequired
};
