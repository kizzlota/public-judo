import {TASKS_INFO, TASKS_INFO_ACTIVE, UPDATE_TASK, ADD_TASK, DELL_TASK} from '../../../const/PageInfo';

export let taskData = (tasks = []) => {
    return {type: TASKS_INFO, tasks};
};

export let changeInfo = (id) => {
    return {type: TASKS_INFO_ACTIVE, id};
};

export let updateTask = (id, new_data) => {
    return {type: UPDATE_TASK, ...{id, new_data}};
};

export let deleteTask = (id) => {
    return {type: DELL_TASK, id};
};

export let addTask = (new_data) => {
    return {type: ADD_TASK, new_data};
};
