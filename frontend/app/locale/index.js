import en from './en.json';
import ua from './ua.json';
import  HomePageLocale from '../pages/Home/locale';
import  AboutPageLocale from '../pages/Task/locale';
import  ErrorsPageLocale from '../pages/404/locale';
import  UserLocales from '../components/user/locale';
import  loginForm from '../components/loginForm/locale';
import  inputsElements from '../components/inputs/locale';
import  bodyLocale from '../components/body/locale';
import  taskLocale from '../components/taskContainer/locale';

export default {
    en: {
        ...en,
        HomePageLocale: HomePageLocale.en,
        AboutPageLocale: AboutPageLocale.en,
        ErrorsPageLocale: ErrorsPageLocale.en,
        UserLocales: UserLocales.en,
        loginForm: loginForm.en,
        inputsElements: inputsElements.en,
        bodyLocale: bodyLocale.en,
        taskLocale: taskLocale.en
    },
    ua: {
        ...ua,
        HomePageLocale: HomePageLocale.ua,
        AboutPageLocale: AboutPageLocale.ua,
        ErrorsPageLocale: ErrorsPageLocale.ua,
        UserLocales: UserLocales.ua,
        loginForm: loginForm.ua,
        inputsElements: inputsElements.ua,
        bodyLocale: bodyLocale.ua,
        taskLocale: taskLocale.ua
    }
};
