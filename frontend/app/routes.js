import React from 'react';
import HomeRoute from './pages/Home/routers/index';
import AboutRoute from './pages/Task/routers/index';
import ErrorRoute from './pages/404/routers/index';
import {Switch} from 'react-router-dom';

export default (
    <div id="bodyHome">
        <Switch>
            {HomeRoute}
            {AboutRoute}
            {ErrorRoute}
        </Switch>
    </div>
);
