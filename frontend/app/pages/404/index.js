import '../allStyle/index.scss';
import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import BodyContainer from '../../components';
import User from '../../components/user/index';
import {GLOBAL} from '../../config/index';
import {PUBLICK_STATUS} from '../../const/PageInfo';

class ErrorPage extends Component {
    static Path = '*';
    static access_status = PUBLICK_STATUS;
    static pageName = '404';
    updateSwitch = true;

    constructor(props) {
        super(props);
        let state = {
            showMenu: false
        };
        GLOBAL.createPathLocale(this.Path);
        this.state = state;
    }

    componentWillMount() {
        let {dispatch} = this.props;
        let {changePageName, updateLocales} = GLOBAL;
        updateLocales(dispatch, ErrorPage.pageName);
        if (!changePageName(dispatch, ErrorPage.pageName)) {
            console.log('error change pageName ' + ErrorPage.pageName);
        }
        this.updateSwitch = false;
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (!this.updateSwitch) {
            this.updateSwitch = true;
            return false;
        }
        return true;
    }

    setParamBody() {
        let bodyParam = ::this.getParamBody();
    }

    getCenterContent() {
        return (
            <div className="home-welcome">
                <header><h2>This is an open context page</h2></header>
                <section>
                    <p>404 NOT FOUND</p>
                </section>
            </div>
        );
    }

    getHeaderContent() {
        return <User/>;
    }

    getParamBody() {
        let {getParamsColumns} = GLOBAL;
        let headerParam = getParamsColumns('header');
        let CenterContainerParam = getParamsColumns();
        let FooterParam = getParamsColumns('footer');
        headerParam.activeLink = ErrorPage.pageName;
        CenterContainerParam.child = ::this.getCenterContent();
        headerParam.child = ::this.getHeaderContent();
        FooterParam.activeLink = ErrorPage.pageName;
        return {
            headerParam,
            CenterContainerParam,
            FooterParam
        };
    }

    render() {
        let paramsBody = ::this.getParamBody();
        return (
            <div className="containerPage">
                <BodyContainer
                    {...paramsBody}
                />
            </div>
        );
    }
}

ErrorPage.PropTypes = {
    listMenu: PropTypes.array.isRequired
};

function mapStateToProps(state) {
    return {
        locale: state.langPage.ErrorPage
    };
}

export default connect(mapStateToProps)(ErrorPage);
