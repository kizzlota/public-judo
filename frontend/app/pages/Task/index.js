import './style/index.scss';
import React, {Component} from 'react';
import {connect} from 'react-redux';
import BodyContainer from '../../components';
import PropTypes from 'prop-types';
import is from 'is_js';
import {GLOBAL} from '../../config/index';
import {PROTECTED_STATUS} from '../../const/PageInfo';
import User from '../../components/user/index';
import TaskInfo from '../../components/taskContainer/taskInfo';
import TascContainer from '../../components/taskContainer';
import * as Const from '../../const/PageInfo';


class TodosPage extends Component {
    static Path = '/todos';
    static access_status = PROTECTED_STATUS;
    static animated_script = 'home';
    static pageName = 'todos';


    constructor(props) {
        super(props);
        let state = {
            showMenu: false
        };
        this.state = state;
    }

    static getInfo() {
        let {getLocalePage} = GLOBAL;
        let locale = getLocalePage('AboutPageLocale');
        return {
            namePage: locale.header.title,
            value: TodosPage.pageName
        };
    }

    getHeaderContent() {
        return <User/>;
    }

    getLeftColumnContent() {
        return <TascContainer/>;
    }

    getCenterContent() {
        return <TaskInfo/>;
    }

    componentDidMount() {
        let {dispatch} = this.props;
        let {changePageName} = GLOBAL;
        dispatch({type: Const.SWITCH_MENU, switcherMenu: true});
        if (!changePageName(dispatch, TodosPage.pageName)) {
            console.error('error change pageName ' + TodosPage.pageName);
        }
    }

    getParamBody() {
        let {getParamsColumns} = GLOBAL;
        let headerParam = getParamsColumns('header');
        let LeftContainerParam = getParamsColumns();
        let CenterContainerParam = getParamsColumns();
        // let RightContainerParam = getParamsColumns();
        let FooterParam = getParamsColumns('footer');
        headerParam.activeLink = this.pageName;
        headerParam.child = ::this.getHeaderContent();
        LeftContainerParam.child = ::this.getLeftColumnContent();
        LeftContainerParam.class_add = 'animated fadeInLeft';
        CenterContainerParam.child = ::this.getCenterContent();
        FooterParam.activeLink = this.pageName;
        return {
            headerParam,
            LeftContainerParam,
            CenterContainerParam,
            // RightContainerParam,
            FooterParam
        };
    }

    render() {
        let paramsBody = ::this.getParamBody();
        let {locale} = this.props;
        if (is.undefined(locale) || !locale) {
            let {getLocalePage} = GLOBAL;
            locale = getLocalePage('AboutPageLocale');
        }
        return (
            <div className="containerPage">
                <BodyContainer
                    {...paramsBody}
                />
            </div>
        );
    }
}

TodosPage.PropTypes = {
    listMenu: PropTypes.array.isRequired
};

function mapStateToProps(state) {
    return {
        locale: state.langPage.AboutPageLocale
    };
}

export default connect(mapStateToProps)(TodosPage);
