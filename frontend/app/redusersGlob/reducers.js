import {combineReducers} from 'redux';
import {routerReducer} from 'react-router-redux';
import * as Const from '../const/PageInfo';
import Locales from '../locale/index';
import loginFormTougle from '../components/loginForm/redusers';
import statusAuthUser from '../components/user/redusers';
import statusTask from '../components/taskContainer/redusers';

let initialStatePageInfo = {
    pageName: ''
};

let initialStateLang = {
    langPage: {}
};


function pageInfo(state = initialStatePageInfo, action) {
    switch (action.type) {
        case Const.CHANGE_PAGE: {
            let {pageName} = action;
            return {pageName};
        }
        default:
            return state;
    }
}

function localeInfo(state = initialStateLang, action) {
    switch (action.type) {
        case Const.CHANGE_LANG: {
            let {lang} = action;
            return {...Locales[lang]};
        }
        default:
            return state;
    }
}

function swithcMenuList(state = false, action) {
    switch (action.type) {
        case Const.SWITCH_MENU: {
            return action.switcherMenu;
        }
        default:
            return state;
    }
}

export default combineReducers({
    routing: routerReducer,
    pageInfo: pageInfo,  // ...AppReducer
    langPage: localeInfo,
    switcherMenu: swithcMenuList, // перемиканя сторінок з анімацією
    userData: statusAuthUser,
    statusFormLogin :loginFormTougle,
    Tasks:statusTask
});
