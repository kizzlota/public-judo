import {createStore, applyMiddleware, compose} from 'redux';
import rootReducer from './redusersGlob/reducers';
import { composeWithDevTools } from 'redux-devtools-extension';

let isProduction = process.env.NODE_ENV === 'production';
function _applyMiddleware() {
    const middleware = [
    ];
    return applyMiddleware(...middleware);
}

export default function configureStore(initialState) {
    let store = null;
    if (!isProduction)
        store = compose(composeWithDevTools(applyMiddleware(_applyMiddleware)))(createStore)(rootReducer, initialState);
    else
        store = compose(applyMiddleware(_applyMiddleware))(createStore)(rootReducer, initialState);
    return store;
}
