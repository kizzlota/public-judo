const entry = require('./webpack/const/entry');
const output = require('./webpack/const/output');
const template = require('./webpack/const/template');
const merge = require('webpack-merge');
const jsWatcher = require('./webpack/jsWatcher');
const cssWatcher = require('./webpack/cssWatcher');
const scssWatcher = require('./webpack/scssWatcher');
const devServer = require('./webpack/devServer');
const imagesWatcher = require('./webpack/imagesWatcher');
const fontWatcher = require('./webpack/fontWatcher');
const Plugins = require('./webpack/Plugins');
const common = merge([
    {
        context: __dirname,
        devtool: 'source-map',
        entry: entry + '/index.js',
        output:{
            jsonpFunction:'webpackJsonp',
            path:output,
            publicPath: '/',
            filename:'static/js/[name].js'
        },
    },
    jsWatcher(),
    imagesWatcher(),
    fontWatcher(),
]);


module.exports = function(env){
    if(env === 'development'){
        return merge(
            [
                common,
                scssWatcher(env),
                Plugins(env),
                devServer()
            ]
        );
    }
    if(env === 'production'){
        return merge(
            [
                common,
                scssWatcher(env),
                cssWatcher(),
                Plugins(env),
            ]);
    }

};
