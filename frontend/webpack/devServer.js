const entry = require('./const/entry');
module.exports = function(){
    return {
        devServer: {
            stats: {
                assets: true,
                children: false,
                chunks: false,
                hash: false,
                modules: false,
                publicPath: false,
                timings: true,
                version: false,
                warnings: true,
                colors: {
                    green: '\u001b[32m',
                    },
            },
            contentBase: entry,
            port: 3031,
            historyApiFallback: true,
            inline: true,
            hot: true,
            host: '127.0.0.1'
        }
    }
};
