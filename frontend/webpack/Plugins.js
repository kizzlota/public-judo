const output = require('./const/output');
const template = require('./const/template');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
module.exports = function(env) {
    const isProd = env === 'production';
    const plugins = [
        new webpack.optimize.CommonsChunkPlugin({
            name:"common",
            minChunks: Infinity
        }),
        new HtmlWebpackPlugin({
            title:'my React Task',
            template: template,
            filename: 'index.html',
            path: output,
            chunks:'common',
            production: isProd,
            inject: true,
            minify: isProd && {
                removeComments: true,
                collapseWhitespace: true,
                removeRedundantAttributes: true,
                useShortDoctype: true,
                removeEmptyAttributes: true,
                removeStyleLinkTypeAttributes: true,
                keepClosingSlash: true,
                minifyJS: true,
                minifyCSS: true,
                minifyURLs: true,
            }
        }),
        new ScriptExtHtmlWebpackPlugin({
            defaultAttribute: 'async',
            preload: {
                test: /^0|^main|^style-.*$/,
                chunks: 'all',
            },
        }),
    ];
    if(isProd) {
        process.env.NODE_ENV = 'production';
        plugins.push(
            new webpack.DefinePlugin({
                'process.env': {
                      NODE_ENV: JSON.stringify('production'),
                      BABEL_ENV: JSON.stringify('production')
                }
            }),
            new ExtractTextPlugin({
                filename:'static/css/[name].css',
                allChunks: true
            }),
            new UglifyJSPlugin({
                source_map:true,
                compress: {
                    warnings: false,
                    screw_ie8: true,
                    conditionals: true,
                    unused: true,
                    comparisons: true,
                    sequences: true,
                    dead_code: true,
                    evaluate: true,
                    if_return: true,
                    join_vars: true,
                },
            })
        );
    } else {
        plugins.push(
            new webpack.DefinePlugin({
                'process.env': {
                    NODE_ENV: JSON.stringify('development'),
                    BABEL_ENV: JSON.stringify('development')
                }
            }),
            new webpack.HotModuleReplacementPlugin(),
            new webpack.NoEmitOnErrorsPlugin(),
            new webpack.NamedModulesPlugin()
        );
    }
    return {
        plugins:plugins
    }
};
