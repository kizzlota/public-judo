# Judolaunch test task

This is the application that make todo or tasks tracker. you can create, update , list and delete task

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
python >= 3.7
docker
 
```
## Preparing for install project
You need make settings in project file settings.py

#### Regular django application
* first way to use application with regular django template language:
```
USE_REACT_JS = False
```
* second way is if you want to use reactJS, make changes in setting.py file: 

```
USE_REACT_JS = True
```
* in case local start app recommended to make your own virtualenviroment 

* and if you want to start app without docker you need to set postgres database settings and of course install postresql locally

```
 DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': 'your_db_name',
            'USER': 'your_user',
            'PASSWORD': 'your_passw',
            'HOST': 'localhost',
            'PORT': '5432',
        }
    }
```
* in case start app local need to make migrations and create superuser manually
```
 (yourvenv)user@ubuntu:~/work/app$ python maange makemigrations 
 (yourvenv)user@ubuntu:~/work/app$ python maange migrate 
 (yourvenv)user@ubuntu:~/work/app$ python maange createsuperuser
```
* install packages from requirements.txt
```
 pip install -r requirements.txt
```

#### Installing with docker


* [Docker on ubuntu](
https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-18-04) - the detailed instuctions


Also need to add user to docker group sudo usermod -aG docker ${USER}

###Installing Docker-compose on ubuntu

* [Docker-compose](https://docs.docker.com/compose/install/#install-compose) - The docker compose install instuctions

### Docker instruction
* change wirking directory to place where docker file is located and run the command:

```
 docker-compose up --build 
```

### After succesfull docker install
* username: admin@admin.com
* password: User_1234567890
* url: ipaddress:current_macine_ip:port:8080 (example http://192.168.0.2:8080/)

### Built With

* [Django](https://docs.djangoproject.com/en/2.0/releases/2.0/) - The web framework used
* [DRF](http://www.django-rest-framework.org/) - RESTful framework
* [REACTjs](https://reactjs.org/) - FrontEnd part
* [Docker](https://www.docker.com/) - Docker


## Authors

* **kizzlota** - *Initial work* - [gitlab](https://gitlab.com/kizzlota)




