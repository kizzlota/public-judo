from django.urls import path
from user_profiles.views import BasicView, AccountLogin, AccountLogout

from django.conf.urls import handler404, handler500, handler403

urlpatterns = [
    path('', BasicView.as_view(), name='main'),
    path('login/', AccountLogin.as_view(), name='login'),
    path('logout/', AccountLogout.as_view(), name='logout'),

]

handler403 = 'views.error_403_view'
handler404 = 'views.error_404_view'
handler500 = 'views.error_500_view'