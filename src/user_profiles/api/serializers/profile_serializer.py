from rest_framework import serializers
from user_profiles.models import *


class ProfileSerializer(serializers.ModelSerializer):
    first_name = serializers.SerializerMethodField()
    second_name = serializers.SerializerMethodField()
    last_name = serializers.SerializerMethodField()

    class Meta:
        model = UserProfile
        fields = ('id', 'first_name', 'second_name', 'last_name', 'user',
                  'phone', 'description', 'user')

    def get_first_name(self, obj):
        first_name = obj.user.first_name
        return first_name

    def get_second_name(self, obj):
        second_name = obj.user.second_name
        return second_name

    def get_last_name(self, obj):
        last_name = obj.user.last_name
        return last_name

    def update(self, instance, validated_data):
        instance.save()
        instance.user.first_name = self.context.get('first_name', instance.user.first_name)
        instance.user.last_name = self.context.get('last_name', instance.user.last_name)
        instance.user.second_name = self.context.get('second_name', instance.user.second_name)
        instance.user.save()

        instance.date_of_birth = validated_data.get('date_of_birth', instance.date_of_birth)
        instance.phone = validated_data.get('phone', instance.phone)
        instance.description = validated_data.get('description', instance.description)
        instance.save()
        return instance
