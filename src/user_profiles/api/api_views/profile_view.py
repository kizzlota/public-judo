from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
from django.db.models import Q

# own imports
from user_profiles.api.serializers.profile_serializer import *


class ProfileView(viewsets.ViewSet):
    permission_classes = (IsAuthenticated,)

    def create(self, request):
        exist = UserProfile.objects.filter(user=request.user).exists()
        if exist:
            return Response({'ERROR': "profile is already exists"}, status=status.HTTP_400_BAD_REQUEST)
        else:
            serializer = ProfileSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save(user=request.user)
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                print(serializer.errors)
            return Response({serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

    def retrieve(self, request):
        try:
            user_profile = UserProfile.objects.get(user=request.user)
        except UserProfile.DoesNotExist as e:
            return Response({'ERROR': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        else:
            serializer = ProfileSerializer(instance=user_profile)
            if serializer:
                return Response(serializer.data, status=status.HTTP_200_OK)
            return Response({"ERROR": "no data in the profile"}, status=status.HTTP_204_NO_CONTENT)

    def update(self, request):
        context = {}
        context['first_name'] = request.data.get('first_name', None)
        context['last_name'] = request.data.get('last_name', None)
        context['second_name'] = request.data.get('second_name', None)

        profile = UserProfile.objects.get(user=request.user)
        serializer = ProfileSerializer(instance=profile, data=request.data, context=context)
        if serializer.is_valid():
            serializer.save(user=request.user)
            return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id):
        try:
            UserProfile.objects.get(Q(id=id) | Q(user__email=request.user)).delete()
            return Response({"OK": "deleted"}, status=status.HTTP_200_OK)
        except UserProfile.DoesNotExist as e:
            return Response({'ERROR': str(e)}, status=status.HTTP_400_BAD_REQUEST)
