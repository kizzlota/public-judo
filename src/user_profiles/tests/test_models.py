from django.test import TestCase, Client
from django.utils import timezone
from django.urls import reverse
from user_profiles.forms import *
from user_profiles.models import *


class UserTestCase(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create(email='test@test.com', first_name='tester',
                                        password='test_user_secret1234567890')
        self.user.save()
        self.client.login(username='test_user', password='test_user_secret1234567890')

    def tearDown(self):
        self.user.delete()


