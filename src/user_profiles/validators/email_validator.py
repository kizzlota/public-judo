from django.core.exceptions import ValidationError


def email_validator(email):
    if not '@' in email:
        raise ValidationError('@ must be in email')
    return email
