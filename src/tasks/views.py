from django.views.generic import ListView, CreateView, DetailView, DeleteView
from django.views import View
from django.shortcuts import render, redirect
from tasks.models import Task
from .forms import TaskForm
from django.http import HttpResponseRedirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse
from django.shortcuts import get_object_or_404
from django.http import Http404
from django.urls import reverse_lazy
from django.contrib import messages
import emoji
from django.core.exceptions import PermissionDenied

# Create your views here.


class AllTasks(ListView):
    template_name = 'tasks_tmpl/all_tasks.html'
    model = Task

    def get(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated:
            return redirect(reverse('login'))
        else:
            statuses = ['open', 'done']
            queryset = Task.objects.filter(status__in=statuses).order_by('id').reverse()
            return render(request, 'tasks_tmpl/all_tasks.html', {'object_list': queryset})


class CreateTask(CreateView):
    model = Task
    fields = ['name', 'description', 'status']
    form = TaskForm()
    template_name = 'tasks_tmpl/add_tasks.html'
    success_url = '/'

    def get(self, request, *args, **kwargs):
        if self.request.user.is_authenticated:
            form = self.get_form()
            return render(request, 'tasks_tmpl/add_tasks.html', {'form': form})
        else:
            return redirect(reverse('login'))

    def post(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated:
            return redirect(reverse('login'))
        else:
            form = self.get_form()
            if form.is_valid():
                return self.form_valid(form)
            else:
                return self.form_invalid(form)

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.author = self.request.user
        self.object.save()
        # success message sending
        messages.success(request=self.request, message="{} Task was succesfully created".format(
            emoji.emojize(':smile: :sunglasses:', use_aliases=True)),
                         extra_tags='alert alert-danger form-group" role="alert')
        return HttpResponseRedirect(reverse('task_all'))

    def form_invalid(self, form):
        messages.error(request=self.request,
                       message='Task was not created, {}'.format(emoji.emojize(':weary: :pensive:', use_aliases=True)))
        return HttpResponseRedirect(reverse('task_create'))


class DetailTask(DetailView, LoginRequiredMixin):
    model = Task
    context_object_name = 'task'
    template_name = 'tasks_tmpl/detail_task.html'


class UpdateTask(View, LoginRequiredMixin):
    model = Task
    fields = ['name', 'description', 'status']
    template_name = 'tasks_tmpl/edit_task.html'

    def get(self, request, *args, **kwargs):
        task_id = kwargs.get('id')
        task_instance = Task.objects.get(id=task_id)
        form = TaskForm(instance=task_instance)
        return render(request, 'tasks_tmpl/edit_task.html', {'form': form})

    def post(self, request, *args, **kwargs):
        task_id = kwargs.get('id')
        instance = get_object_or_404(Task, id=task_id)
        form = TaskForm(request.POST, instance=instance)
        if form.is_valid():
            firm_obj = form.save(commit=False)
            firm_obj.updated_by = request.user
            firm_obj.save()
            messages.success(request=self.request, message="{} Task was succesfully updated".format(
                emoji.emojize(':thumbsup:', use_aliases=True)), extra_tags='alert alert-danger form-group" role="alert')
            return redirect(reverse('task_all'))
        return Http404("Error occured during update task")


class DoneTask(View, LoginRequiredMixin):
    model = Task

    def get(self, request, *args, **kwargs):
        task_id = kwargs.get('id')
        instance = get_object_or_404(Task, id=task_id)
        instance.status = 'done'
        instance.updated_by = request.user
        instance.save()
        messages.success(request=self.request, message="{} Task was marked as 'done' ".format(
            emoji.emojize(':heavy_check_mark:', use_aliases=True)),
                         extra_tags='alert alert-danger form-group" role="alert')
        return redirect(reverse('task_all'))


class MyDoneTask(View, LoginRequiredMixin):

    def get(self, request, *args, **kwargs):
        queryset = Task.objects.filter(status='open')
        if queryset.exists():
            return render(request, 'tasks_tmpl/all_tasks.html', {'object_list': queryset})


class ShowDoneTask(View, LoginRequiredMixin):
    def get(self, request, *args, **kwargs):
        queryset = Task.objects.all()
        if queryset.exists():
            return render(request, 'tasks_tmpl/all_tasks.html', {'object_list': queryset})


class DeleteTask(DeleteView, LoginRequiredMixin):
    model = Task
    success_url = reverse_lazy('task_all')
    template_name = 'tasks_tmpl/delete_task.html'

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.object.author == request.user:
            self.object.delete()
            messages.success(request=self.request, message="{} Task was successfully deleted ".format(
                emoji.emojize(':toilet: :fire:', use_aliases=True)),
                             extra_tags='alert alert-danger form-group" role="alert')
            return HttpResponseRedirect(self.get_success_url())
        else:

            raise PermissionDenied
