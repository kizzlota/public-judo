from django.db import models
from django.conf import settings
from user_profiles.models import User, UserProfile
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.utils.text import slugify
from django.urls import reverse

# Create your models here.
STATUS_TASK = (
    ('open', 'OPEN'),
    ('done', 'DONE'),
)


class Task(models.Model):
    """
    task model
    """

    name = models.CharField(max_length=250, blank=True, null=True, verbose_name='name of the task', db_index=True)
    slug = models.SlugField(max_length=50, unique=True)
    date_of_task = models.DateTimeField(auto_now_add=True, null=True)
    description = models.TextField(max_length=500, blank=True, null=True)
    status = models.CharField(max_length=50, default='open', choices=STATUS_TASK)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, related_name='author',
                               verbose_name='the author of the task', on_delete=models.CASCADE, db_index=True)
    created = models.DateTimeField(auto_now_add=True, verbose_name='created')
    updated = models.DateTimeField(auto_now=True, verbose_name='updated')
    updated_by = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, related_name='updater',
                                   verbose_name='update by', on_delete=models.CASCADE, db_index=True)

    def get_absolute_url(self):
        return reverse('task_detail', kwargs={'pk': self.id})

    def __str__(self):
        return str(self.name)


@receiver(pre_save, sender=Task)
def slugger(instance, **kwargs):
    slug = slugify(instance.name)
    exists = Task.objects.filter(slug=slug).exists()
    if exists:
        slug = f'{slugify(instance.name)}-{instance.id}'
    instance.slug = slug
