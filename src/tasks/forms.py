from django import forms
from django.forms import ModelForm
from django.core.exceptions import ValidationError
from .models import Task


class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = ['id', 'name', 'description', 'status']
        exclude = ['slug', 'author', 'date_of_task', 'created', 'updated', 'updated_by', ]

    def clean_name(self):
        name = self.cleaned_data.get('name')
        if len(name) < 3:
            raise ValidationError('name of the todo is less than 3 signs', code='invlalid')
        return name

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['name'].widget.attrs.update({'class': 'form-control col-md-4'})
        self.fields['description'].widget.attrs.update({'class': 'form-control col-md-4'})
        self.fields['status'].widget.attrs.update({'class': 'form-control col-md-4'})
