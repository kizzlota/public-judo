from django.contrib import admin
from .models import *


# Register your models here.


@admin.register(Task)
class UserAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'slug', 'date_of_task', 'description', 'status', 'author', 'updated', 'updated_by',
                    'created']
    search_fields = ['id', 'name', 'status']
    list_filter = ['id', 'status']
    list_editable = ['name', 'slug', 'status']
