from django.urls import path, include
from tasks.views import AllTasks, CreateTask, UpdateTask, DetailTask, DoneTask, MyDoneTask, DeleteTask
from tasks.api.views.task_view import TaskViewSet

api_task_urls = [
    path('task/create/', TaskViewSet.as_view({'post': 'create'}), name='create_task'),
    path('task/delete/', TaskViewSet.as_view({'delete': 'delete'}), name='delete_task'),
    path('task/retrieve/<id>/', TaskViewSet.as_view({'get': 'retrieve'}), name='retrieve_task'),
    path('task/update/<id>/', TaskViewSet.as_view({'put': 'update'}), name='update_task'),
    path('task/all/', TaskViewSet.as_view({'get': 'list'}), name='list_task'),
]


urlpatterns = [
    path('all/', AllTasks.as_view(), name='task_all'),
    path('add/', CreateTask.as_view(), name='task_create'),
    path('edit/<int:id>/', UpdateTask.as_view(), name='task_edit'),
    path('detail/<pk>/', DetailTask.as_view(), name='task_detail'),
    path('mark_done/<id>/', DoneTask.as_view(), name='task_done'),
    path('my_done_tasks/', MyDoneTask.as_view(), name='task_marked_done'),
    path('delete/<pk>/', DeleteTask.as_view(), name='task_delete'),

    #api urls
    path('api/', include(api_task_urls))
]
