from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
from tasks.api.serializers.task_serializer import *


class TaskViewSet(viewsets.ViewSet):
    permission_classes = (IsAuthenticated,)

    def list(self, request):
        context = {}
        context['updater_info'] = True
        if request.user.is_authenticated:
            tasks = Task.objects.all()
            if tasks:
                serializer = TaskSerializer(tasks, many=True, context=context)
                return Response(serializer.data, status=status.HTTP_200_OK)
            else:
                return Response({'ERROR': 'no tasks to display'}, status=status.HTTP_204_NO_CONTENT)
        return Response({'ERROR': 'please login'}, status=status.HTTP_401_UNAUTHORIZED)

    def create(self, request):
        context = {}
        context['first_name'] = request.user.first_name if request.user.first_name else None
        context['last_name'] = request.user.last_name if request.user.last_name else None
        context['second_name'] = request.user.second_name if request.user.second_name else None

        serializer = TaskSerializer(data=request.data, context=context)

        if serializer.is_valid():
            serializer.save(author=request.user)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, id):
        context = {}
        context['updater_info'] = True
        context['first_name'] = request.user.first_name
        context['last_name'] = request.user.last_name
        context['second_name'] = request.user.second_name
        context['user'] = request.user

        try:
            tasks = Task.objects.get(id=id)
        except Task.DoesNotExist as e:
            return Response({'ERROR': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        serializer = TaskSerializer(instance=tasks, data=request.data, context=context)
        if serializer.is_valid():
            serializer.save(updated_by=request.user)
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def retrieve(self, request, id):
        context = {}
        context['updater_info'] = True
        try:
            tasks_object = Task.objects.get(id=id, author=request.user)
        except Task.DoesNotExist as e:
            return Response({'ERROR': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        serializer = TaskSerializer(instance=tasks_object, context=context)
        if serializer:
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response({'ERROR': ' tasks is not found'}, status=status.HTTP_204_NO_CONTENT)

    def delete(self, request, id):
        try:
            task_instance = Task.objects.get(id=id, author=request.user)
        except Task.DoesNotExist as e:
            return Response({'ERROR': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        else:
            task_instance.delete()
            return Response({"OK": "successfully deleted"}, status=status.HTTP_200_OK)
